FROM registry.windenergy.dtu.dk/ellipsys/dockerimages/centos76_intel2021.2

RUN python3.9 -m venv py3

RUN /bin/bash -c 'source /opt/intel/oneapi/setvars.sh && \
    source py3/bin/activate && \
    pip install Cython && \
    wget https://github.com/numpy/numpy/releases/download/v1.21.2/numpy-1.21.2.tar.gz && \
    tar -xzf numpy-1.21.2.tar.gz && \
    cd numpy-1.21.2 && \
    echo "[mkl] \
library_dirs = $MKLROOT/lib/intel64 \
include_dirs = $MKLROOT/include \
mkl_libs = mkl_rt \
lapack_libs =" > site.cfg && \
    python setup.py config --compiler=intelem build_clib --compiler=intelem build_ext --compiler=intelem install && \
    cd .. && \
    pip install coverage scipy pandas xarray && \
    pip install mpi4py -v'
RUN rm -rf numpy-1.21.2*

WORKDIR /home/gitlab-runner

